let field = document.body.querySelector('input');
let val = '';
let logg = false;
let numbers = [...document.body.querySelectorAll('.number')];
let operations = [...document.body.querySelectorAll('.pink')];
let memory = '';
let memoryEmpty = true;
let from7to9 = []
from7to9[0] = numbers[0]
from7to9[1] = numbers[1]
from7to9[2] = numbers[2]
let from4to6 = []
from4to6[0] = numbers[3]
from4to6[1] = numbers[4]
from4to6[2] = numbers[5]
let from1to3 = []
from1to3[0] = numbers[6]
from1to3[1] = numbers[7]
from1to3[2] = numbers[8]
let zero = numbers[9]
let coma = document.body.querySelector('.coma')
let reset = document.body.querySelector('.reset');
let mrc = document.body.querySelector('.mrc');
let mMinus = document.body.querySelector('.mMinus');
let mPlus = document.body.querySelector('.mPlus');
for (let i = 0; i < 3; i++) {
    from1to3[i].addEventListener('click', () => {
        click(i + 1);
    })
}
for (let i = 0; i < 3; i++) {
    from4to6[i].addEventListener('click', () => {
        click(i + 4);
    })
}
for (let i = 0; i < 3; i++) {
    from7to9[i].addEventListener('click', () => {
        click(i + 7);
    })
}
for (let y = 0; y < 4; y++) {
    operations[y].addEventListener('click', () => {
        clickop(y)
    })
}
zero.addEventListener('click', () => {
    click(0)
})
coma.addEventListener('click', () => {
    click('.')
})
reset.addEventListener('click', resett);
mPlus.addEventListener('click', memoryAdd);
mMinus.addEventListener('click', memoryRemove);
mrc.addEventListener('click', memoryLog);
function memoryAdd() {
    if (memoryEmpty == true) {
        memory = val;
        memoryEmpty = false;
    }
}
function memoryRemove() {
    if (memoryEmpty == false) {
        memory = ''
        memoryEmpty = true;
    }
}
function memoryLog() {
    if (memoryEmpty == false && logg == false) {
        field.value = val + memory;
        val = field.value;
        logg = true;
    }
    if (memoryEmpty == false && logg == true) {
        memoryRemove();
        logg = false
    }
}
function resett() {
    field.value = '';
    val = '';
}
function click(number) {
    field.value = val + '' + number;
    val = field.value;
}
function clickop(operation) {
    let k = 0;
    for (let g = 0; g < 4; g++) {
        if (operation == g) {
            k = g;
        }
    }
    if (k == 0) {
        field.value = val + '/';
        val = field.value;
    }
    if (k == 1) {
        field.value = val + '*';
        val = field.value;
    }
    if (k == 2) {
        field.value = val + '-';
        val = field.value;
    }
    if (k == 3) {
        field.value = val + '+';
        val = field.value;
    }
}
let total = document.body.querySelector('.orange');
total.addEventListener('click', totals);
function totals() {
    let terms = field.value.split('+');
    let subtractors = field.value.split('-');
    let multiplier = field.value.split('*');
    let dividers = field.value.split('/')
    if (terms.length > 1) {
        field.value = +terms[0] + +terms[1];
        val = field.value;
    }
    if (subtractors.length > 1) {
        field.value = +subtractors[0] - +subtractors[1];
        val = field.value;
    }
    if (multiplier.length > 1) {
        field.value = +multiplier[0] * +multiplier[1];
        val = field.value;
    }
    if (dividers.length > 1) {
        field.value = +dividers[0] / +dividers[1];
        val = field.value;
    }
}
window.addEventListener('keyup', (event) => {
    keyUp(event)
})
function keyUp(event) {
    digitt('0', 0);
    digitt('1', 1);
    digitt('2', 2);
    digitt('3', 3);
    digitt('4', 4);
    digitt('5', 5);
    digitt('6', 6);
    digitt('7', 7);
    digitt('8', 8);
    digitt('9', 9);
    digitt('/', '/');
    digitt('*', '*');
    digitt('-', '-');
    digitt('+', '+');
    function digitt(eventt, number) {
        if (event.key == eventt) {
            click(number);
        }
    }
    if (event.code == 'Space') {
        memoryLog();
    }
    if (event.code == 'KeyM') {
        memoryAdd();
    }
    if (event.code == 'Delete') {
        memoryRemove();
    }
    if (event.code == 'Backspace') {
        resett();
    }
    if (event.code == 'Enter') {
        totals();
    }
}